var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.render('profile/profile', { title: 'Qslay' });
});

module.exports = router;
