const express = require('express');
const router = express.Router();
const passport = require('passport');


/* GET users listing. */
router.get('/', function (req, res, next) {
    res.render('signup', { title: 'Signup Qslay' });
});

router.post('/', passport.authenticate('local.signup', { failWithError: true, failureFlash: true }),
    function (req, res) {
        console.log(req);
        if (req.xhr) {
            return res.json(req.user);
        }
        return res.json('/profile');
    },
    function (err, req, res, next) {
        // if (req.xhr) { return json(req.session.errors); }
        console.log(err);
        return res.redirect('/');
    }
);

// router.post('/', function(req, res){
//     res.send(req.body);
// });

module.exports = router;