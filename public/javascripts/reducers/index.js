/**
 * index.js - contains references to all the reducers
 */

import {
    combineReducers
} from 'redux';

import reducers_1 from './reducers_1.js';

export default combineReducers({
    action_set_1 : reducers_1
});