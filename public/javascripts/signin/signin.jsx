'use strict'

import React from 'react';
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText
} from 'reactstrap';


class Signin extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <Form>
          <FormGroup>
            <Label for="email">Email</Label>
            <Input type="email" name="email" id="email" />
          </FormGroup>
          <FormGroup>
            <Label for="password">Password</Label>
            <Input type="password" name="password" id="password" />
          </FormGroup>
          <Button>Signin</Button>
        </Form>
      </>
    )
  }
}

export default Signin;