"use strict"

require("babel-polyfill");

import React from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap'

//Page
import Profile from './profile/profile.jsx';
import NavigationBar from './nav/nav.jsx';
import Signup from './signup/signup.jsx';
import Signin from './signin/signin.jsx';

//Mount target
const profileDiv = document.getElementById('profile-div');
const navLocation = document.getElementById('navbar-location');
const signupLocation = document.getElementById('signup-div');
const signinLocation = document.getElementById('signin-div');


if (navLocation) {
    ReactDOM.render(
        <NavigationBar />,
        navLocation
    )
}


if (profileDiv) {
    ReactDOM.render(
        <Profile />,
        profileDiv
    );
}

if (signupLocation) {
    ReactDOM.render(
        <Signup />,
        signupLocation
    )
}

if (signinLocation) {
    ReactDOM.render(
        <Signin />,
        signinLocation
    )
}